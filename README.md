# ESIB Rally Paper 2019 -- Online Challenge

This repository contains all projects related to the 2019 edition of the ESIB Rally Paper Online Challenge. The challenge consists of a text adventure, during which the player stumbles upon various puzzles and riddles. Every now and then, the riddle the player has to solve is hidden inside on the four other games contained in this repository (every game is on a different branch). The text adventure is written with Inform7, and is deployed using Flask and Heroku. The four games are 2D games written in PyGame.

## Text adventure
![](res/rpg.png)

## First game: Snake
![](res/snake.gif)

## Second game: UnlockMe
![](res/unlockme.png)

## Third game: GridZ
![](res/image-grid.png)

## Fourth game: Ninja run
![](res/ninjarun.gif)
